#!/usr/bin/env php
<?php

/*
 *	APPLICATION TITLE: BT-TRACKER-MINDER
 *  PURPOSE: Use to update, merge, clean up, and sort for output a list of torrent-tracker URLs.
 *	AUTHOR: Jim S. Smith
 *	COPYRIGHT: (c)2023 - A FRESH WEB SOLUTION, Licensed, with attribution, under GPL 2.0 or later.
 *
 *	VERSION: 1.0.1d, Updated: 2023/11/09
 *
 *  DESCRIPTION: An application - update, merge, clean up, and sort for output a list of
 *  torrent-tracker URLs. Great for maintaining up-to-date "tracker lists" for your torrent
 *  client(s). Trackers to be removed will be listed in a file called "trackers.remove".
 *
 *  - USE AT YOUR OWN RISK!
 *
 *  REQUIREMENTS:
 *
 *  1. PHP (CLI) version of at least 7.3 or greater.
 *
 */


//	MUST have at least one file-path argument.
if ( count( $argv ) < 2 ) {
	$_basename = basename( $argv[0] );

	fwrite( STDERR, <<< _ERROR_
--> Required argument(s) missing!

IF only ONE argument is used, then THAT file is simply cleaned up and resorted.

IF multiple arguments are used, the first filepath is the created output file, and
   all of the following filepaths are MERGED, cleaned, and sorted - before they
   are written to the output file.

EXAMPLE: $_basename ./new-file.txt ./my-old-file.txt ./add-this-1.txt ./add-this-2.asc

NOTE:    To remove a tracker URL from the current list, enter it within a file called "trackers.remove".


_ERROR_
	);

	die( 9 );
	}

//	If we can not read nor create the target file, then we may as well stop right here.
if ( ! file_exists( $argv[1] ) && ! touch( $argv[1] ) ) {
	fwrite( STDERR, "* * I/O ERROR: File '{$argv[1]}' does not exist AND could not be created! * *\n\n" );
	die( 2 );
	}


//	Retrive and then clean-validate trackers list from a file.
function get_TrackerData( $_file ) {
//	If we can not read the file, or it is actually empty, return an empty array.
	if ( ! file_exists( $_file ) || ! ( $theData = file( $_file ) ) || ! count( $theData ) ) {
		return [];
		}

	$newData = [];

	foreach ( $theData as $thisItem ) {
		$thisItem = trim( $thisItem );

//		Ignore all blank lines, comments, and malformed URLs.
		if ( empty( $thisItem ) || ! preg_match( '!^(https?|udp)://!', $thisItem ) ) {
			continue;
			}

		$newData[] = $thisItem;
		}

	return $newData;
}

//	'trackers.remove' is an optional file with the list of trackers to remove from the main list.
function remove_from_TrackerData( $_this_array = [] ) {
	if ( file_exists( './trackers.remove' ) && ( $removeList = get_TrackerData( './trackers.remove' ) ) && count( $removeList ) ) {
		fwrite( STDOUT, "* * FOUND: 'trackers.remove' file. If no longer needed, please delete it. * *\n" );

		$newList = [];
		$removeCount = 0;

		foreach ( $_this_array as $thisTracker ) {
			if ( in_array( $thisTracker, $removeList ) ) {
				$removeCount++;
				continue;
				}

			$newList[] = $thisTracker;
			}

		fwrite( STDOUT, "* * Non-working trackers removed: ( $removeCount ). * *\n\n" );
		}

	else {
		return $_this_array;
		}

	return $newList;
}


//	Check if we have more than one file-path given for arguments.
if ( count( $argv ) > 2 ) {
	$newCount = 2;
	$outFile = [];
	$sourceFiles = count( $argv );

//	Merge all the data from the other files into one array.
	for ( $i = $newCount; $i < $sourceFiles; $i++ ) {
		if ( file_exists( $argv[$i] ) ) {
			fwrite( STDOUT, "Merging '{$argv[$i]}'.\n" );
			$outFile = array_merge( $outFile, get_TrackerData( $argv[$i] ) );
			}

		else {
			fwrite( STDERR, "I/O ERROR: * * File '{$argv[$i]}' does not exist! * *\n" );
			}
		}

	fwrite( STDOUT, "\n" );
	$outFile = array_unique( $outFile );
	}

//	Only update THIS file by cleaning, resorting, and removing bad trackers from it.
elseif ( count( $argv ) == 2 ) {
	fwrite( STDOUT, "Cleaning and sorting trackers data in '{$argv[1]}'.\n\n" );
	$outFile = get_TrackerData( $argv[1] );
	}


//	Finish processing the final trackers list.
sort( $outFile, SORT_NATURAL );
$outFile = remove_from_TrackerData( $outFile );
$final_count = count( $outFile );

$outData = implode( "\n\n", $outFile );
$outData = trim( preg_replace( [ "'\r\n'", "'\r'" ], "\n", $outData ) );


//	Write the updated trackers list to the target file.
if ( file_put_contents( "{$argv[1]}", "$outData\n" ) ) {
	fwrite( STDOUT, "* * Wrote ( $final_count ) records out to: '{$argv[1]}'. * *\n\n" );
	die( 0 );
	}

else {
	fwrite( STDOUT, "* * I/O ERROR: Could not write records out to: '{$argv[1]}'! * *\n\n" );
	die( 2 );
	}


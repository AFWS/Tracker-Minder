# Tracker-Minder

* Copyright (c) 2023 - Jim S. Smith, AFWS
* Licensed under GPL 2.0 or newer.


** PURPOSE: **

Make things simple when adding or updating your BitTorrent client "trackers list"!


** HOW TO SET UP / INSTALL: **

Just copy the executable script to your systems "/usr/bin" folder or similar, and make sure to set the "X" ( "executable" ) bits with something like "chmod +X /usr/bin/trackers.php".


** HOW TO USE: **

A very simple way to manage your torrent-trackers list. Automatically merges your "add" files, removes duplicates within the list,

And, if a file called "trackers.remove" exists in the same work directory, removes THOSE listed URLs from your new, merged list.


** STEPS TO UPDATE YOUR TORRENT-CLIENT'S TRACKERS LIST: **

1. Locate and click on your client's "settings" or "preferences" menu option.

2. Find the option for adding/updating trackers ( expect that this may be under the generic heading of "BitTorrent" or similar ).

3. Your trackers list will usually be located on the setting labeled "Automatically add these trackers to new torrents/downloads",
   ( Which will probably be accompanied by a "textbox" input - containing the list of trackers [ if already provided by default ] ).

4. "Copy" the entire list [ Using "CTRL-"A" & CTRL-"C" ] to a new text file, like "my-trackers.txt" for example.

5. "Copy" your downloaded list of new torrent trackers to another new text file, like "add-trackers.txt" for example.

6. If there are any torrents you wish to have "pruned" from your list after all the merging operations have completed,
   You will need to place these trackers to be removed into a text file named "trackers.remove". You can delete this file when
   you no longer need it.

7. The commandline syntax then is: "trackers.php ./new-trackers.txt ./my-trackers.txt ./add-trackers.txt" ( Examples given: You may use other filenames. )

8. If there were no errors, you should see that your new trackers list file has been generated ( and notice of how many "records" were written ).

9. Open the new trackers file and copy the contents OVER the list of trackers in your torrent client's "add trackers" input box.

   You should notice that your trackers list contains only unique tracker URLs, and are separated by a blank line. This is advised to leave it formatted "as is".

10. Most likely required, restart your torrent client - and from there on, all newly-added torrents should also use this list of trackers, merged with whichever trackers the torrent uses.


** REQUIRES: **

* PHP-CLI minimum version of 7.3 or greater is recommended. ( Not yet tested for PHP 8+. )

* Ability to execute basic file-system functions like "file(), file_get_contents(), file_put_contents()".


